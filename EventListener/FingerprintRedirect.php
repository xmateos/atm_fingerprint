<?php

namespace ATM\FingerprintBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class FingerprintRedirect{

    private $router;
    private $session;
    private $config;

    public function __construct(RouterInterface $router, SessionInterface $session, $atm_fingerprint_config){
        $this->router = $router;
        $this->session = $session;
        $this->config = $atm_fingerprint_config;
    }

    public function onKernelRequest(GetResponseEvent $event){
        if(!$event->getRequest()->isXmlHttpRequest()) {
            if ($this->session->has('atm_fingerprint_redirect')) {
                $this->session->remove('atm_fingerprint_redirect');
                $this->session->set('atm_fingerprint_custom_redirect', true);

                if($this->config['fingerprint_error_redirect_route_name']){
                    $route = $this->router->generate($this->config['fingerprint_error_redirect_route_name']);
                    $response = new RedirectResponse($route);
                    $event->setResponse($response);
                }
            }
        }
    }
}