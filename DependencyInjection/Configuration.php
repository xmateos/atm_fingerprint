<?php

namespace ATM\FingerprintBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $treeBuilder
            ->root('atm_fingerprint')
                ->children()
                    ->scalarNode('api_base_url')->isRequired()->end()
                    ->scalarNode('fingerprint_error_redirect_route_name')->defaultValue(false)->end()
                    ->scalarNode('site_name')->isRequired()->end()
                    ->scalarNode('error_message')->isRequired()->end()
                    ->booleanNode('use_assetic')->defaultValue(false)->end()
                    ->booleanNode('active')->defaultValue(true)->end()
                ->end()
            ->end()
        ;
        return $treeBuilder;
    }
}
