<?php

namespace ATM\FingerprintBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class StatusSuccess extends Event{

    const NAME = 'atm_fingerprint_success.event';

    protected $user;
    protected $response;
    protected $fingerprint_data;

    public function __construct($user,$response,$fingerprint_data)
    {
        $this->user = $user;
        $this->response = $response;
        $this->fingerprint_data = $fingerprint_data;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getResponse(){
        return $this->response;
    }

    public function getFingerprintData()
    {
        return $this->fingerprint_data;
    }
}