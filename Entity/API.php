<?php

namespace ATM\FingerprintBundle\Entity;

class API
{

    public function __get($attr)
    {
        return self::$$attr;
    }

    public static function process($params)
    {
        $api_base_url = $params['api_base_url'];

        $params = json_encode($params);

        $url = $api_base_url.'process/';

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: '.strlen($params)
        ));

        $response = curl_exec($ch);
        if(curl_error($ch))
        {
            return curl_error($ch);
        }
        curl_close($ch);
        return json_decode($response, true);
    }

    public static function insert($params){
        $api_base_url = $params['api_base_url'];

        $params = json_encode($params);

        $url = $api_base_url.'insert/';

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: '.strlen($params)
        ));

        $response = curl_exec($ch);
        if(curl_error($ch))
        {
            return curl_error($ch);
        }
        curl_close($ch);
        return json_decode($response, true);

    }

    public static function getResponse()
    {
        $params = stripslashes(file_get_contents("php://input"));
        return json_decode($params, true);
    }
}